package com.nitka;

import com.nitka.exceptions.NoSuchPossibleSumException;
import com.nitka.util.AlgorithmUtil;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import static com.nitka.util.AlgorithmUtil.NUMBER_OF_DELETION;
import static com.nitka.util.AlgorithmUtil.filterOnlyTopSums;
import static junit.framework.TestCase.*;

public class AlgorithmUtilTest {

    @Test
    public void testfilterOnlyTopSums() throws Exception {

        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
        List<Integer> topNumbers = filterOnlyTopSums(numbers);

        int sum = topNumbers.stream().mapToInt(Integer::intValue).sum();

        assertTrue(topNumbers.size() == NUMBER_OF_DELETION);
        assertTrue(sum == 34);
    }

    @Test
    public void testGetArrayOfNumbersFromString() throws Exception {

        String line="1 2 3";
        List<Integer> numbers = AlgorithmUtil.getArrayOfNumbersFromString(line);

        int sum = numbers.stream().mapToInt(Integer::intValue).sum();

        assertSame(numbers.size(), 3);
        assertSame(sum, 6);
    }

    @Test
    public void testGetAllSumsForNextLine() throws Exception {

        List<Integer> nextLineNumbers = Arrays.asList(8,9,10);
        List<Integer> topSum = Arrays.asList(1,2,3,4);
        List<Integer> allSumsForNextLine = AlgorithmUtil.getAllSumsForNextLine(topSum,nextLineNumbers);

        int sum = allSumsForNextLine.stream().mapToInt(Integer::intValue).sum();

        assertSame(allSumsForNextLine.size(), 12);
        assertTrue(sum ==246);
    }

    @Test
    public void testGetAllSumsForNextLineWithEmptyTopSum() throws Exception {

        List<Integer> nextLineNumbers = Arrays.asList(1,2,3);
        List<Integer> topSum = new ArrayList<>();
        List<Integer> allSumsForNextLine = AlgorithmUtil.getAllSumsForNextLine(topSum,nextLineNumbers);

        int sum = allSumsForNextLine.stream().mapToInt(Integer::intValue).sum();

        assertSame(allSumsForNextLine.size(), 3);
        assertSame(sum , 12);
    }

    @Test
    public void testGetMaxNumberWithoutRemainder() throws Exception {

        List<Integer> numbers = Arrays.asList(1,2,3,4);
        int result = AlgorithmUtil.getMaxNumberWithoutRemainder(numbers);

        assertSame(result ,4);
    }

    @Test(expected = NoSuchPossibleSumException.class)
    public void testGetMaxNumberWithoutRemainderNegative() throws Exception {
        List<Integer> numbers = Arrays.asList(1,2,3);
        int result = AlgorithmUtil.getMaxNumberWithoutRemainder(numbers);
    }
}
