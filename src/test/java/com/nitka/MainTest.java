package com.nitka;

import com.nitka.exceptions.NoSuchPossibleSumException;
import org.junit.Test;

import static junit.framework.TestCase.assertSame;
import static junit.framework.TestCase.assertTrue;

public class MainTest {
    @Test
    public void testRunWithPositiveTestCase() throws Exception {

        String testPositive = "8 3 4\n" +
                "4 8 12\n" +
                "9 5 6\n" +
                "2 8 3\n" +
                "12 3 5 \n" +
                "1 4 12";

        int result = AlgorithmMain.processAlgorithm(testPositive);

        assertSame(result, 88);
    }

    @Test
    public void testRunWithPositiveTestCase2() throws Exception {

        String testPositive = "8 3 4\n" +
                "4 8 12\n" +
                "9 5 6\n" +
                "2 8 3\n" +
                "12 3 5 \n" +
                "1 4 12\n" +
                "8 3 4\n" +
                "4 8 12\n" +
                "9 5 6\n" +
                "2 8 3\n" +
                "12 3 5 \n" +
                "1 4 12\n" +
                "8 3 4\n" +
                "4 8 12\n" +
                "9 5 6\n" +
                "2 8 3\n" +
                "12 3 5 \n" +
                "1 4 12\n" +
                "8 3 4\n" +
                "4 8 12\n" +
                "9 5 6\n" +
                "2 8 3\n" +
                "12 3 5 \n" +
                "1 4 12\n";

        int result = AlgorithmMain.processAlgorithm(testPositive);

        assertTrue(364==result);
    }

    @Test(expected = NoSuchPossibleSumException.class)
    public void testRunWithNegativeTestCase() throws Exception {

        String testNegative = "6 1 8\n";
        AlgorithmMain.processAlgorithm(testNegative);

    }
}
