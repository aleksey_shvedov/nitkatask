package com.nitka.util;

import com.nitka.exceptions.NoSuchPossibleSumException;

import java.util.ArrayList;
import java.util.List;

public class AlgorithmUtil {

    public static final String SPLIT_PATTERN = "\\s+";
    public static final int NUMBER_OF_DELETION = 4;


    public static int getMaxNumberWithoutRemainder(List<Integer> topSums) {
        for(int number : topSums){
            if(number % NUMBER_OF_DELETION == 0 && number != 0){
                return number;
            }
        }
        throw new NoSuchPossibleSumException();
    }

    public static List<Integer> filterOnlyTopSums(List<Integer> newLineSum) {
        int[] topSums = new int[NUMBER_OF_DELETION];
        for(Integer sum : newLineSum){
            int remainder = sum % NUMBER_OF_DELETION;
            if(topSums[remainder] == 0 || topSums[remainder] < sum){
                topSums[remainder]=sum;
            }
        }
        return convertToList(topSums);
    }

    private static List<Integer> convertToList(int[] topSums) {
        List<Integer> intList = new ArrayList<>(topSums.length);
        for (int i : topSums){
            intList.add(i);
        }
        return intList;
    }

    public static  List<Integer> getAllSumsForNextLine(List<Integer> topSums, List<Integer> numbersFromLine) {
        if(topSums.isEmpty()){
            return getPossibleSumsFromLine(numbersFromLine);
        }
        List<Integer> newLineSum = new ArrayList<>();
        for(int sum: topSums){
            for(int number : getPossibleSumsFromLine(numbersFromLine)){
                newLineSum.add(sum+number);
            }
        }

        return newLineSum;
    }

    private static List<Integer> getPossibleSumsFromLine(List<Integer> numbersFromLine) {
        List<Integer> possibleSums = new ArrayList<>();
        for(int i = 0 ; i < numbersFromLine.size()-1;i++){
            for(int n = i+1 ; n<numbersFromLine.size();n++){
                possibleSums.add(numbersFromLine.get(i)+numbersFromLine.get(n));
            }
        }
        return possibleSums;
    }

    public static List<Integer> getArrayOfNumbersFromString(String line) {
        String[] intStrArray  = line.split(SPLIT_PATTERN);
        List<Integer> intArray = new ArrayList<>();
        for (String intStr : intStrArray) {
            intArray.add(Integer.parseInt(intStr));
        }
        return intArray;
    }
}
