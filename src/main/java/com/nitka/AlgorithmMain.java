package com.nitka;

import com.nitka.exceptions.NoSuchPossibleSumException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.nitka.util.AlgorithmUtil.*;

@SpringBootApplication
public class AlgorithmMain implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(AlgorithmMain.class);

    @Value("${filePath:}")
    private String filePath;

    public static void main(String[] args) {
        SpringApplication.run(AlgorithmMain.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        File file = new File(filePath);

        try {
            String fileText = getTextFromFile(file);
            int result = processAlgorithm(fileText);
            log.info("Result Number which divide on {} without a remainder: {}", NUMBER_OF_DELETION, result);
        }catch (NoSuchPossibleSumException e){
            log.error("there are no such sum which divide without a remainder");
        }catch (NoSuchFileException e){
            log.error("there are no such file : {} ", filePath);
        }catch (Exception e ){
            log.error("wrong format of file");

        }
    }

    public static int processAlgorithm(String fileText) throws Exception{

        List<String> linesFile= Arrays.asList(fileText.split("\n"));
        List<Integer> topSums = new ArrayList<>();
        for(String line:linesFile){
            List<Integer> numbersFromLine = getArrayOfNumbersFromString(line);
            List<Integer> newLineSum = getAllSumsForNextLine(topSums, numbersFromLine);
            topSums = filterOnlyTopSums(newLineSum);
        }
        log.info("Top Sums: {}",topSums);
        return getMaxNumberWithoutRemainder(topSums);
    }

    private String getTextFromFile(File file) throws NoSuchFileException {
        StringBuilder fileText = new StringBuilder();
        try (FileReader fr = new FileReader(file);
             BufferedReader reader = new BufferedReader(fr)){

            List<Integer> topSums = new ArrayList<>();
            String line = reader.readLine();
            while (line != null) {
                fileText.append(line);
                line = reader.readLine();
            }

        } catch (Exception e) {
            throw new NoSuchFileException(e.getMessage());
        }
        return fileText.toString();
    }


}
