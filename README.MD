For run this app : 
 1. build the project : mvn clean install
 2. run command : java -DfilePath=D://temp/file.txt -jar algoritm-task-1.0.0-SNAPSHOT.jar
 
 Mandatory argument : 
 
 **filePath**  - path where the input file located
 
 Example of the Input file located in the root folder of the project: **fileExample.txt**
 
 Structure of the file : 
 3 numbers in one line splitted by space.
 
 App works just with a valid input file.